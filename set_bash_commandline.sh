#
# by WGB
#
# setup my bash command line in my terminal environtment
#
# Disclaimer: some code parts swipped from examples
#

# Try to execute a `return` statement,
# but do it in a sub-shell and catch the results.
# If this script isn't sourced, that will raise an error.
$(return >/dev/null 2>&1)

# What exit code did that give?
if [ "$?" -eq "0" ]
then
	export PS1="\r\n[\u@\W] $:"
else
	echo
    echo "ERROR, you need to run this script with the source command."
    echo
    echo "for example:"
    echo
    echo "source $0"
    echo
fi


###if [[ $_ == "$(type -p "$0")" ]]; then
###    echo I am invoked from a sub shell
###else
###    echo I am invoked from a source command
###fi

