
#!/bin/bash

# ##############################################################################
#
#  Written by: WGB
#
#              Wrapper script for Codeenvy DOCKER from a MACos command line.
#
# ############################################################################


# ##############################################################################
#
# Setup the Persitent Volume for our data
#
# ############################################################################

#
# Unfortunatley, there is a bug on a MACos with Codenvy and mapping a volume
# for the instance or backup that is different than the data
#

MyDataVolume="mydata-PV"

#
# Which IDE do you wan tot run?
#

MyIDEtoRun="eclipse/che"

#MyIDEtoRun="codenvy/cli"



# ###################################################
#      Internal fuctions....
#
#          string to lower case
#          usage: MY_STRING=$(str2lc "SOME STRING")
#
#
#			Taken from somewhere, not written by: wgb
#
# ###################################################
function str2lc()
{
	uppers=ABCDEFGHIJKLMNOPQRSTUVWXYZ
	lowers=abcdefghijklmnopqrstuvwxyz

	LC_OUTPUT=""

    i=0
    while ([ $i -lt ${#1} ]) do

        CUR=${1:$i:1}

        case $uppers in
            *$CUR*)
				CUR=${uppers%$CUR*};LC_OUTPUT="${LC_OUTPUT}${lowers:${#CUR}:1}" ;;
            *)
				LC_OUTPUT="${LC_OUTPUT}$CUR" ;;
        esac

        i=$((i+1))
    done

    echo "${LC_OUTPUT}"
}
# ###################################################

if [ $# -gt 0 ] ;
then
	NewCmd=$(str2lc "$*")
else
	NewCmd=""
fi

if [ "$NewCmd" == "stop" ] ;
then

	echo
	echo "*******************************************"
	echo "***                                     ***"
	echo "*** NOTE: stop command modified to pass ***"
	echo "***       password and admin            ***"
	echo "***                                     ***"
	echo "*******************************************"
	echo

	NewCmd="stop --user admin --password password"
fi

echo
echo Runnng: docker run $MyIDEtoRun
echo

docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock \
               -v $(pwd)/$MyDataVolume:/data \
               $MyIDEtoRun $NewCmd

if [ $# -lt 1 ] ;
then
	echo
	echo
	echo "*******************************************"
	echo "***                                     ***"
	echo "*** ERROR, no command provided.         ***"
	echo "***                                     ***"
	echo "*******************************************"
	echo 
	echo "USAGE:"
	echo 
	echo "$0 <COMMAND>"
	echo 

fi


